var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var morgan = require('morgan');
var cors = require('cors');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var clienteRouter = require('./routes/cliente');
var conceptoRouter = require('./routes/concepto_uso_puntos');
var reglaRouter = require('./routes/regla');
var vencimientoPuntosRouter = require('./routes/vencimiento_puntos');
var bolsaPuntosRouter = require('./routes/bolsa_puntos');
var usoPuntosRouter = require('./routes/uso_puntos');
var consultasRouter = require('./routes/consultas');
var serviciosRouter = require('./routes/servicios');

var app = express();

// Set up mongoose connection

var mongoDB = 'mongodb://localhost:27017/prueba';
mongoose.connect(mongoDB, {
  useNewUrlParser: true
});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(morgan('tiny'));
app.use(cors());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api/cliente', clienteRouter);
app.use('/api/concepto', conceptoRouter);
app.use('/api/regla', reglaRouter);
app.use('/api/vencimiento', vencimientoPuntosRouter);
app.use('/api/bolsa', bolsaPuntosRouter);
app.use('/api/uso', usoPuntosRouter);
app.use('/api/consultas', consultasRouter);
app.use('/api/servicios', serviciosRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
