var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ConceptoUsoPuntoSchema = new Schema({
  descripcion_concepto: {
    type: String,
    required: true,
    maxlength: 100
  },
  puntos_requeridos: {
    type: Number,
    required: true
  }
});

module.exports = mongoose.model('ConceptoUsoPuntos', ConceptoUsoPuntoSchema);
