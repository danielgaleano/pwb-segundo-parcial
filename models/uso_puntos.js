var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var UsoPuntosCabeceraSchema = new Schema({
  cliente: {
    type: Schema.Types.ObjectId,
    ref: 'Cliente',
    required: true
  },
  puntaje_utilizado: {
    type: Number,
    required: true
  },
  fecha: {
    type: Date,
    required: true
  },
  concepto_uso: {
    type: Schema.Types.ObjectId,
    ref: 'ConceptoUsoPuntos',
    required: true
  },
  detalles: [{ type: Schema.Types.ObjectId, ref: 'UsoPuntosDetalle' }]
});

var UsoPuntosDetalleSchema = new Schema({
  cabecera: {
    type: Schema.Types.ObjectId,
    ref: 'UsoPuntosCabecera',
    required: true
  },
  puntaje_utilizado: {
    type: Number,
    required: true
  },
  bolsa: {
    type: Schema.Types.ObjectId,
    ref: 'BolsaPuntos',
    required: true
  }
});

module.exports.UsoPuntosCabecera = mongoose.model('UsoPuntosCabecera', UsoPuntosCabeceraSchema);
module.exports.UsoPuntosDetalle = mongoose.model('UsoPuntosDetalle', UsoPuntosDetalleSchema);
