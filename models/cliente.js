var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ClienteSchema = new Schema({
  nombre: {
    type: String,
    required: true,
    maxlength: 100
  },
  apellido: {
    type: String,
    required: true,
    maxlength: 100
  },
  numero_documento: {
    type: String,
    required: true,
    unique: true,
    maxlength: 50
  },
  tipo_documento: {
    type: String,
    required: true,
    maxlength: 50
  },
  nacionalidad: {
    type: String,
    required: true,
    maxlength: 50
  },
  email: {
    type: String,
    required: true,
    unique: true,
    maxlength: 50
  },
  telefono: {
    type: String,
    required: true,
    maxlength: 50
  },
  fecha_nacimiento: {
    type: Date
  }
});

module.exports = mongoose.model('Cliente', ClienteSchema);
