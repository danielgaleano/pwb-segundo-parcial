var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var BolsaPuntosCabeceraSchema = new Schema({
  cliente: {
    type: Schema.Types.ObjectId,
    ref: 'Cliente',
    required: true
  },
  fecha_asignacion: {
    type: Date,
    required: true
  },
  fecha_caducidad: {
    type: Date,
    required: true
  },
  puntaje_asignado: {
    type: Number,
    required: true
  },
  puntaje_utilizado: {
    type: Number,
    required: true
  },
  saldo_puntos: {
    type: Number,
    required: true
  },
  monto_operacion: {
    type: Number,
    required: true
  }
});

module.exports = mongoose.model('BolsaPuntos', BolsaPuntosCabeceraSchema);
