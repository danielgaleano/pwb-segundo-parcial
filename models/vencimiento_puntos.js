var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var VencimientoPuntosSchema = new Schema({
  fecha_inicio: {
    type: Date,
    required: true
  },
  fecha_fin: {
    type: Date,
    required: true
  },
  dias_duracion: {
    type: Number,
    required: true
  }
});

module.exports = mongoose.model('VencimientoPuntos', VencimientoPuntosSchema);
