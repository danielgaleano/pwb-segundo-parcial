var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ReglaSchema = new Schema({
  limite_inferior: {
    type: Number,
    required: true
  },
  limite_superior: {
    type: Number,
    required: true
  },
  monto_equivalencia_punto: {
    type: Number,
    required: true
  }
});

module.exports = mongoose.model('Regla', ReglaSchema);
