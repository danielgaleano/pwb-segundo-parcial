const Regla = require('../models/regla');

exports.list = async (req, res) => {
  try {
    var result = await Regla.find().exec();
    if (result.length === 0) {
      res.status(404).send({
        message: 'Lista vacía.'
      });
    } else {
      res.send({
        lista: result,
        cantidad: result.length
      });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.view = async (req, res) => {
  try {
    var regla = await Regla.findById(req.params.id).exec();
    if (!regla) {
      res.status(404).send({
        message: 'Regla no encontrada.'
      });
    } else {
      res.send(regla);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.new = async (req, res) => {
  try {
    var regla = new Regla(req.body);
    var result = await regla.save();
    res.send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.update = async (req, res) => {
  try {
    var regla = await Regla.findById(req.params.id).exec();
    if (!regla) {
      res.status(404).send({
        message: 'Regla no encontrada.'
      });
    } else {
      regla.set(req.body);
      var result = await regla.save();
      res.send(result);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.delete = async (req, res) => {
  try {
    var result = await Regla.deleteOne({
      _id: req.params.id
    }).exec();
    if (!result) {
      res.status(404).send({
        message: 'Regla no encontrada.'
      });
    } else {
      res.send(result);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
