const VencimientoPuntos = require('../models/vencimiento_puntos');

exports.list = async (req, res) => {
  try {
    var result = await VencimientoPuntos.find().exec();
    if (result.length === 0) {
      res.status(404).send({
        message: 'Lista vacía.'
      });
    } else {
      res.send({
        lista: result,
        cantidad: result.length
      });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.view = async (req, res) => {
  try {
    var vencimientoPuntos = await VencimientoPuntos.findById(req.params.id).exec();
    if (!vencimientoPuntos) {
      res.status(404).send({
        message: 'Vencimiento no encontrado.'
      });
    } else {
      res.send(vencimientoPuntos);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.new = async (req, res) => {
  try {
    var vencimientoPuntos = new VencimientoPuntos(req.body);
    var result = await vencimientoPuntos.save();
    res.send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.update = async (req, res) => {
  try {
    var vencimientoPuntos = await VencimientoPuntos.findById(req.params.id).exec();
    if (!vencimientoPuntos) {
      res.status(404).send({
        message: 'Vencimiento no encontrado.'
      });
    } else {
      vencimientoPuntos.set(req.body);
      var result = await vencimientoPuntos.save();
      res.send(result);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.delete = async (req, res) => {
  try {
    var result = await VencimientoPuntos.deleteOne({
      _id: req.params.id
    }).exec();
    if (!result) {
      res.status(404).send({
        message: 'Vencimiento no encontrado.'
      });
    } else {
      res.send(result);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
