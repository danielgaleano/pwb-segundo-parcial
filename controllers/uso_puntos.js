const moment = require('moment');
const BolsaPuntos = require('../models/bolsa_puntos');
const UsoPuntos = require('../models/uso_puntos');
const ConceptoUsoPuntos = require('../models/concepto_uso_puntos');
const Email = require('../controllers/mail');


exports.new = async (req, res) => {
  try {
    var conceptoUso = await ConceptoUsoPuntos.findById(req.body.concepto_uso).exec();
    var puntosRequeridos = conceptoUso.puntos_requeridos;
    var fechaActual = moment();

    // Se obtienen las bolsas activas del cliente
    var bolsaPuntosActivas = await BolsaPuntos.find({
      $and: [
        {
          cliente: req.body.cliente
        },
        {
          fecha_caducidad: {
            $gte: fechaActual.toDate()
          }
        },
        {
          saldo_puntos: {
            $gt: 0
          }
        }
      ]
    }).sort({ fecha_asignacion: 'asc' }).exec();

    // Se calcula el total de puntos disponibles
    var totalPuntosDisponibles = bolsaPuntosActivas.reduce((a, b) => a + b.saldo_puntos, 0);
    var resultCabecera;

    // Se verifica si exite la cantidad de puntos necesarios
    if (totalPuntosDisponibles < puntosRequeridos) {
      res.status(404).send({
        message: 'No tiene la cantidad necesaria de puntos.'
      });
    }
    else {
      // Guardar cabecera
      var usoPuntosCabecera = new UsoPuntos.UsoPuntosCabecera();
      usoPuntosCabecera.cliente = req.body.cliente;
      usoPuntosCabecera.puntaje_utilizado = puntosRequeridos;
      usoPuntosCabecera.fecha = fechaActual;
      usoPuntosCabecera.concepto_uso = conceptoUso.id;

      // var resultCabecera = await usoPuntosCabecera.save();

      var bolsaAcumulada = 0;
      // Se itera sobre las bolsas
      for (let i = 0; i < bolsaPuntosActivas.length; i++) {
        const element = bolsaPuntosActivas[i];
        var puntosUtilizados = 0;
        if (bolsaAcumulada < puntosRequeridos) {
          var puntosPendientes = puntosRequeridos - bolsaAcumulada;
          if (element.saldo_puntos <= puntosPendientes) {
            bolsaAcumulada += element.saldo_puntos;
            puntosUtilizados = element.saldo_puntos;
            element.saldo_puntos = 0; // Editar bolsa
            element.puntaje_utilizado = puntosUtilizados;
          } else {
            bolsaAcumulada += puntosPendientes;
            puntosUtilizados = puntosPendientes;
            element.saldo_puntos -= puntosPendientes; // Editar bolsa
            element.puntaje_utilizado = puntosUtilizados;
          }
          // Guardar bolsa
          element.save();
          // Guardar detalle
          var usoPuntosDetalle = new UsoPuntos.UsoPuntosDetalle();
          usoPuntosDetalle.cabecera = usoPuntosCabecera.id;
          // usoPuntosDetalle.cabecera = resultCabecera.id;
          usoPuntosDetalle.puntaje_utilizado = puntosUtilizados;
          usoPuntosDetalle.bolsa = element.id;
          usoPuntosDetalle.save();

          usoPuntosCabecera.detalles.push(usoPuntosDetalle);
          // resultCabecera.detalles.push(usoPuntosDetalle);
          // resultCabecera.save();
          resultCabecera = await usoPuntosCabecera.save();
        } else {
          break;
        }
      }
      res.send(resultCabecera); // se responde con la cabecera guardada
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.list = async (req, res) => {
  try {
    var result = await UsoPuntos.UsoPuntosCabecera.find()
      .populate('detalles').populate('concepto_uso').populate('cliente')
      .exec();
    if (result.length === 0) {
      res.status(404).send({
        message: 'Lista vacía.'
      });
    } else {
      res.send({
        lista: result,
        cantidad: result.length
      });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};


exports.view = async (req, res) => {
  try {
    var usoPuntosCabecera = await UsoPuntos.UsoPuntosCabecera.findById(req.params.id)
      .populate('detalles').populate('concepto_uso').populate('cliente')
      .exec();
    if (!usoPuntosCabecera) {
      res.status(404).send({
        message: 'Uso de puntos no encontrado.'
      });
    } else {
      res.send(usoPuntosCabecera);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
