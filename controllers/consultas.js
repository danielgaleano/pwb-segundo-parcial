const moment = require('moment');
const BolsaPuntos = require('../models/bolsa_puntos');
const UsoPuntos = require('../models/uso_puntos');


exports.listUsoPuntos = async (req, res) => {
  try {
    var query = { $and: [] };
    var result;

    if (!req.query.concepto && !req.query.cliente && !req.query.fechaDesde && !req.query.fechaHasta) {
      query = { };
    }

    if (req.query.concepto) {
      query.$and.push({ concepto_uso: req.query.concepto });
    }
    if (req.query.cliente) {
      query.$and.push({ cliente: req.query.cliente });
    }
    if (req.query.fechaDesde) {
      query.$and.push({
        fecha: {
          $gte: moment(req.query.fechaDesde, 'YYYY-MM-DD').toDate()
        }
      });
    }
    if (req.query.fechaHasta) {
      query.$and.push({
        fecha: {
          $lte: moment(req.query.fechaHasta, 'YYYY-MM-DD').toDate()
        }
      });
    }

    result = await UsoPuntos.UsoPuntosCabecera.find(query)
      .populate('concepto_uso').populate('cliente')
      .exec();

    if (result.length === 0) {
      res.status(404).send({
        message: 'Lista vacía.'
      });
    } else {
      res.send({
        lista: result,
        cantidad: result.length
      });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.listBolsaPuntos = async (req, res) => {
  try {
    var fechaActual = moment();
    var query = {
      $and: [
        {
          /* saldo_puntos: {
            $gt: 0
          } */
        }
      ]
    };
    var result;

    if (!req.query.estado && !req.query.cliente && !req.query.vencimiento) {
      query = {};
    }

    if (req.query.cliente) {
      query.$and.push({
        cliente: req.query.cliente
      });
    }

    if (req.query.estado) {
      if (req.query.estado === 'vencidos') {
        query.$and.push({
          fecha_caducidad: {
            $lt: fechaActual.toDate()
          }
        });
      }
      else if (req.query.estado === 'vigentes') {
        query.$and.push({
          fecha_caducidad: {
            $gte: fechaActual.toDate()
          }
        });
      }
    }

    if (req.query.vencimiento) {
      query.$and.push({
        fecha_caducidad: {
          $lte: fechaActual.clone().add(req.query.vencimiento, 'days').toDate()
        }
      });
      query.$and.push({
        fecha_caducidad: {
          $gte: fechaActual.toDate()
        }
      });
    }

    result = await BolsaPuntos.find(query)
      .populate('cliente')
      .exec();

    if (result.length === 0) {
      res.status(404).send({
        message: 'Lista vacía.'
      });
    } else {
      // Se calcula el total de puntos disponibles
      var totalPuntosDisponibles = result.reduce((a, b) => a + b.saldo_puntos, 0);

      res.send({
        lista: result,
        cantidad: result.length,
        total_puntos: totalPuntosDisponibles
      });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
