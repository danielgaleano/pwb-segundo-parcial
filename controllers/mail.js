const nodeMailer = require('nodemailer');

exports.enviarEmail = function (res, email, nombre, apellido, concepto, puntos) {
  const transporter = nodeMailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      // should be replaced with real sender's account
      user: 'node.pwb@gmail.com',
      pass: '585339dan'
    }
  });
  const mailOptions = {
    // should be replaced with real recipient's account
    to: email,
    subject: 'Canje de puntos',
    body: `${nombre} ${apellido}, usted ha canjeado ${concepto} por ${puntos}.`
  };
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message %s sent: %s', info.messageId, info.response);
  });
  res.writeHead(301, {
    Location: 'index.html'
  });
  res.end();
};
