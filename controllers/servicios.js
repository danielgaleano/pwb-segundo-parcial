const moment = require('moment');
const BolsaPuntos = require('../models/bolsa_puntos');
const UsoPuntos = require('../models/uso_puntos');
const Regla = require('../models/regla');


exports.servicioPuntos = async (req, res) => {
  try {
    if (req.query.monto) {
      var regla = await Regla.find({
        $and: [{
          limite_inferior: {
            $lte: req.query.monto
          }
        },
        {
          limite_superior: {
            $gte: req.query.monto
          }
        }
        ]
      }).exec();

      if (regla.length === 0) {
        res.status(404).send({
          message: 'No existe regla de asignación de puntos para el monto de la operación.'
        });
      } else {
        // Calcular cantidad de puntos
        var cantidadPuntos = Math.floor(
          req.query.monto / regla[0].monto_equivalencia_punto
        );
        res.send({
          puntos: cantidadPuntos
        });
      }
    }
    else {
      res.status(400).send({
        message: 'No se ingresó ningún monto.'
      });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
