const Cliente = require('../models/cliente');

exports.list = async (req, res) => {
  try {
    var result = await Cliente.find().exec();
    if (result.length === 0) {
      res.status(404).send({
        message: 'Lista vacía.'
      });
    } else {
      res.send({
        lista: result,
        cantidad: result.length
      });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.view = async (req, res) => {
  try {
    var cliente = await Cliente.findById(req.params.id).exec();
    if (!cliente) {
      res.status(404).send({
        message: 'Cliente no encontrado.'
      });
    } else {
      res.send(cliente);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.new = async (req, res) => {
  try {
    var cliente = new Cliente(req.body);
    var result = await cliente.save();
    res.send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};
