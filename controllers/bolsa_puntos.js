const moment = require('moment');
const BolsaPuntos = require('../models/bolsa_puntos');
const VencimientoPuntos = require('../models/vencimiento_puntos');
const Regla = require('../models/regla');


exports.new = async (req, res) => {
  try {
    var fechaActual = moment();

    var vencimientoPuntos = await VencimientoPuntos.find({
      $and: [
        {
          fecha_inicio: {
            $lte: fechaActual.toDate()
          }
        },
        {
          fecha_fin: {
            $gte: fechaActual.toDate()
          }
        }
      ]
    }).exec();

    var regla = await Regla.find({
      $and: [
        { limite_inferior: { $lte: req.body.monto_operacion } },
        { limite_superior: { $gte: req.body.monto_operacion } }
      ]
    }).exec();

    if (vencimientoPuntos.length === 0) {
      res.status(404).send({
        message: 'No existe periodo de vencimiento para la fecha actual.'
      });
    } else if (regla.length === 0) {
      res.status(404).send({
        message: 'No existe regla de asignación de puntos para el monto de la operación.'
      });
    } else {
      // Calcular fecha de caducidad
      var diasDuracion = vencimientoPuntos[0].dias_duracion;
      var fechaCaducidad = fechaActual.clone().add(diasDuracion, 'days');

      // Calcular cantidad de puntos asignados
      var cantidadPuntosAsignados = Math.floor(
        req.body.monto_operacion / regla[0].monto_equivalencia_punto
      );

      var bolsaPuntos = new BolsaPuntos();
      bolsaPuntos.cliente = req.body.cliente;
      bolsaPuntos.fecha_asignacion = fechaActual;
      bolsaPuntos.fecha_caducidad = fechaCaducidad;
      bolsaPuntos.puntaje_asignado = cantidadPuntosAsignados;
      bolsaPuntos.puntaje_utilizado = 0;
      bolsaPuntos.saldo_puntos = cantidadPuntosAsignados;
      bolsaPuntos.monto_operacion = req.body.monto_operacion;

      var result = await bolsaPuntos.save();
      res.send(result);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.list = async (req, res) => {
  try {
    var result = await BolsaPuntos.find().populate('cliente').exec();
    if (result.length === 0) {
      res.status(404).send({
        message: 'Lista vacía.'
      });
    } else {
      res.send({
        lista: result,
        cantidad: result.length
      });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.view = async (req, res) => {
  try {
    var bolsaPuntos = await BolsaPuntos.findById(req.params.id).populate('cliente').exec();
    if (!bolsaPuntos) {
      res.status(404).send({
        message: 'Bolsa de puntos no encontrada.'
      });
    } else {
      res.send(bolsaPuntos);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
