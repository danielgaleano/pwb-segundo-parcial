const ConceptoUsoPuntos = require('../models/concepto_uso_puntos');

exports.list = async (req, res) => {
  try {
    var result = await ConceptoUsoPuntos.find().exec();
    if (result.length === 0) {
      res.status(404).send({
        message: 'Lista vacía.'
      });
    } else {
      res.send({
        lista: result,
        cantidad: result.length
      });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.view = async (req, res) => {
  try {
    var concepto = await ConceptoUsoPuntos.findById(req.params.id).exec();
    if (!concepto) {
      res.status(404).send({
        message: 'Concepto no encontrado.'
      });
    } else {
      res.send(concepto);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.new = async (req, res) => {
  try {
    var concepto = new ConceptoUsoPuntos(req.body);
    var result = await concepto.save();
    res.send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.update = async (req, res) => {
  try {
    var concepto = await ConceptoUsoPuntos.findById(req.params.id).exec();
    if (!concepto) {
      res.status(404).send({
        message: 'Concepto no encontrado.'
      });
    } else {
      concepto.set(req.body);
      var result = await concepto.save();
      res.send(result);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.delete = async (req, res) => {
  try {
    var result = await ConceptoUsoPuntos.deleteOne({ _id: req.params.id }).exec();
    if (!result) {
      res.status(404).send({
        message: 'Concepto no encontrado.'
      });
    } else {
      res.send(result);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
