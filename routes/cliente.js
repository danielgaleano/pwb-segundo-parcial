var express = require('express');
var router = express.Router();

const clienteController = require('../controllers/cliente');

router.get('/', clienteController.list);
router.get('/:id', clienteController.view);
router.post('/', clienteController.new);


module.exports = router;
