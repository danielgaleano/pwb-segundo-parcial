var express = require('express');
var router = express.Router();

const bolsaPuntosController = require('../controllers/bolsa_puntos');

router.get('/', bolsaPuntosController.list);
router.get('/:id', bolsaPuntosController.view);
router.post('/', bolsaPuntosController.new);

module.exports = router;
