var express = require('express');
var router = express.Router();

const usoPuntosController = require('../controllers/uso_puntos');

router.get('/', usoPuntosController.list);
router.get('/:id', usoPuntosController.view);
router.post('/', usoPuntosController.new);


module.exports = router;
