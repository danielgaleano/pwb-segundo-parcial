var express = require('express');
var router = express.Router();

const consultasController = require('../controllers/consultas');

router.get('/uso/', consultasController.listUsoPuntos);
router.get('/bolsa/', consultasController.listBolsaPuntos);

module.exports = router;
