var express = require('express');
var router = express.Router();

const serviciosController = require('../controllers/servicios');

router.get('/puntos/', serviciosController.servicioPuntos);

module.exports = router;
