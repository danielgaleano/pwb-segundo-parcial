var express = require('express');
var router = express.Router();

const conceptoController = require('../controllers/concepto_uso_puntos');

router.get('/', conceptoController.list);
router.get('/:id', conceptoController.view);
router.post('/', conceptoController.new);
router.put('/:id', conceptoController.update);
router.delete('/:id', conceptoController.delete);


module.exports = router;
