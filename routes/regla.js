var express = require('express');
var router = express.Router();

const reglaController = require('../controllers/regla');

router.get('/', reglaController.list);
router.get('/:id', reglaController.view);
router.post('/', reglaController.new);
router.put('/:id', reglaController.update);
router.delete('/:id', reglaController.delete);


module.exports = router;
