var express = require('express');
var router = express.Router();

const vencimientoPuntosController = require('../controllers/vencimiento_puntos');

router.get('/', vencimientoPuntosController.list);
router.get('/:id', vencimientoPuntosController.view);
router.post('/', vencimientoPuntosController.new);
router.put('/:id', vencimientoPuntosController.update);
router.delete('/:id', vencimientoPuntosController.delete);


module.exports = router;
